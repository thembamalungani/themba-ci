<?php

class ValidatePasswordTest extends PHPUnit_Framework_TestCase
{
  public function testValidaPasswordLength()
  {
    $passwordValidator = new PasswordValidator();
    $this->assertFalse( $passwordValidator->validatePasswordLength('123456') );
  }
}
