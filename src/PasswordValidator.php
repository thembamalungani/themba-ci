<?php

class PasswordValidator
{
   const MIN_LENGTH = 5;
   const MAX_LENGTH = 15;

   public function validatePasswordLength( $password )
   {
     $passwordLength = strlen( $password );
     return  ($passwordLength >= self::MIN_LENGTH) && $password <= (self::MAX_LENGTH);    
   }
}
